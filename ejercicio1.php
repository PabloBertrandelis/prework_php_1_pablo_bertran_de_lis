<?php
    function dimeDia($indice) {
        $diasSemana = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
        if ($indice > 7 || $indice <= 0) {
            return ("Día de la semana incorrecto");
        } else {
            $indice--;
            return $diasSemana [$indice];
        }
    }
    $dia = 7;
    $nombreDia = dimedia($dia);
    echo "El $dia"."º día de la semana es: $nombreDia.\n";
?>